# Sprites
## Table of Contents
* [hud_](#hud_)
* [common](#common)
* [xhair](#xhair)

### READ THIS BEFORE DOWNLOAD
**There is different sprites for different screen resolution! Right now I uploaded sprites for 1920x1080 and more resolution! If you play with resolution lower than 1920x1080 then DO NOT DOWNLOAD THESE SPRITES!**

### [Download link](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/sprites/sprites.zip)

## hud_
![](../../img/sprites/hud/hud_.png)

**Important!!** Hud spites is tightly coupled with all hud txt files! So if you want to use my hud sprites you should copy txt files as well!

* hud_numbers_ - numbers is used for hp, armor and ammo.
* hud_weapon_selection_ - these sprites is rendered on weapon selection menu.
* hud_halflife_logo_ - these sprites is used for rendering half life logo.
* hud_damage_ - these sprites is used for rendering different types of damage(toxic, fire, gas, drowing and etc).
* hud_item_ - these sprites is used for rendering pickup of battery, medkit and longjump.
* hud_deathmsg_ - these sprites is used with death message.
* hud_ammo_ - these sprites is used for pickups.
* 640_pain - these sprites is used when player take damage.
* hud_weapon_normal_ - these sprites is on weapon selection menu, weapon pickups and hud_weapon cvar.

This is my hud sprites. I made them with one purpose - to make half life look more like quake.

##### hud_ installation:
Put all of the hud_ *.spr files from sprites/hud/ to your half-life/valve/sprites(half-life/ag/sprites). (DON'T FORGET *.TXT FILES!)

## common
![](../../img/sprites/common/common.png)

Why common? Cause you can use these sprites with every screen resolution. Many of these sprites is made with black additive texture so they render fully transparent. And the others:

* hotglow - is used to render gauss shoot particles.
* zerogxplode - is used to render explosion.
* laserdot - is used to render rpg laser dot.
* smoke - is used to render gauss beam and rpg rocket smoke.
* xbeam1- is used to render egon beam.

##### common installation:
Put all of the common *.spr files from sprites/common/ to your half-life/valve/sprites(half-life/ag/sprites).

## xhair
![](../../img/sprites/hud/xhair.png)

**ONLY FOR 1920x1080 and MORE RESOLUTION!**
And this is my crosshairs sprites. Again these sprites tightly coupled with *.txt files that you can find in sprites/hud/ do if you want to use them don't forget to download all of the hud files.

##### common installation:
Put all of the common *.spr files from sprites/xhair/ to your half-life/valve/sprites(half-life/ag/sprites).
