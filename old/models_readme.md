# Models
## Table of Contents
* [v_](#v_)
* [p_](#p_)
* [w_](#w_)
* [misc](#misc)
* [player](#player)

### [All Models](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/models/models.zip)
### [Player_1 Models](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/models/player_1.zip)
### [Player_2 Models](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/models/player_2.zip)
### [Q3 Style W Models](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/models/q3_style_w_models.zip)

## v_
![](../../img/models/std_transparent_colored/v_/v_ingame_text.png)

v_ models is used to display player view weapon models.

All of the v_ models use additive/transparent texture.

This is my v_ models. I used rammy's v_ models as a base to build my own v_ models.
There is 2 versions of v_ models in my pack. I use first version with replaced models from v2. Why? Cause I like some v_ models rendered and some of them don't. You can use r_drawviewmodel 1 or 0 for that purpose but there is some bug's with r_drawviewmodel. Cause of that I created full additive (transparent) models for some of the v_ models. Sooo if you wanna use exactly the same v_ models here an installation guide:

##### v_ installation:
Put all of the *.mdl files from models/std_transparent_colored/v_ to your half-life/valve/models(half-life/ag/models). Do the same with *.mdl files in models/std_transparent_colored_v2/v_

## p_
![](../../img/models/std_transparent_colored/p_/p_ingame_text.png)

![](../../img/models/std_transparent_colored/p_/p_text.png)

All of the p_ models don't use additive/transparent texture.

p_ models is used to display weapon models of other players on the server.

I use them with green and green2 models mostly. But these models could be used with red and blue models as well.

##### p_ installation:
Put all of the *.mdl files from models/std_transparent_colored/p_ to your half-life/valve/models(half-life/ag/models).

## w_
![](../../img/models/std_transparent_colored/w_/w_.png)

![](../../img/models/std_transparent_colored/w_/w_mdl_text.png)

w_ models is used to display weapon and misc models on the ground.

So as you can see I use models converted from sprites with vp_parallel texture but some of these models is normal *.mdl files with additive/transparent texture.

Right now I use [q3_style_w_models](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/models/q3_style_w_models.zip) for w_357ammobox, w_shotbox, w_crossbowclip, w_rpgammo, w_gaussammo, w_9mmhandgun, w_357, w_9mmar, w_shotgun, w_crossbow, w_rpg, w_gauss, w_egon, w_hgun, w_argrenade, w_longjump, w_battery.

##### w_ installation:
Put all of the *.mdl files from models/std_transparent_colored/w_ to your half-life/valve/models(half-life/ag/models).

## misc
![](../../img/models/std_transparent_colored/misc/misc_text.png)

This is some models that will make game look a bit altogether. Shell.mdl and shotgunshell.mdl using full additive/transparent texture.

##### misc installation:
Put all of the *.mdl files from models/std_transparent_colored/misc to your half-life/valve/models(half-life/ag/models).

## player
![](../../img/models/player/player_text.png)

This is rammy's models but with new texture. For enemy models I use green2 and for team models I use pink.

![](../../img/models/player/new_player_models.png)

Right now I use models in player_1.zip. I use cl_forceenemymodles for enemy model and cl_forceteammodels for team model.

##### player installation:
Put players directories from models/player to your half-life/valve/models/player (half-life/ag/models/player).
