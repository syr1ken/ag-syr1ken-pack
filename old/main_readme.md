# syr1ken's half-life & adrenaline gamer pack
## syr1ken models, syr1ken model, syr1ken textures, syr1ken sprites, syr1ken sprite

## Table of Contents
* [Description](#description)
* [Models](#models)
* [Sprites](#sprites)
* [Textures](#textures)
* [Gfx](#gfx)
* [Configs](#configs)

## Description
![](img/description_pic.png)

Hello! I'm Dmitriy Granyov more known as syr1ken in half-life and adrenaline gamer community. Many people asking me what textures, models, sprites do I use, so I decided to create this repo for all of my stuff. Below you will see many sprites, models, configs and some other stuff I use. Many of that I made by myself and many I just downloaded somewhere. Big thanks to a really great player rammy who gave me some sort of inspiration to create my own sprites and models. And yea ... let's go.

In this repo you can find 2 directories:
* release
* src

Release directory is used for uploading last versions of my pack. Src directory is used for source code.

**PLEASE READ [THIS](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/sprites#read-this-before-download) BEFORE DOWNLOADING MY SPRITES.**
### Models
* [Models overview & installation](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/tree/master/releases/models#models)
* [Download link](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/models/models.zip)

### Sprites
* [Sprites overview & installation](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/tree/master/releases/sprites)
* [Download link](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/sprites/sprites.zip)

### Textures
* [Textures overview & installation](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/tree/master/releases/textures)
* [Download link](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/textures/textures.zip)

### Gfx
* [Gfx overview & installation](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/tree/master/releases/gfx)
* [Download link](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/gfx/gfx.zip)

### Configs
* [Configs overview & installation](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/tree/master/releases/cfgs#configs)
