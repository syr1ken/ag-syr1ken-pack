# Configs

This is my configuration files for half life and adrenaline gamer. Don't mind default.cfg, autoexec.cfg, violence.cfg and config.cfg cause I put them here as a backup files in case I lose them.

Here you can find two directories. Openag and Bugfixed. I used to play with openag dll but now I use bugfixed dll. So if you use [openAG](https://openag.pro/) dll then you need to download cfgs in openag directory. And if you use [bugfixed](https://github.com/tmp64/BugfixedHL-Rebased) dll then you need to download files from bugfixed directory.

userconfig.cfg is the file where I put all of my configuration. Use my config with caution cause I play with [bugfixed](https://github.com/tmp64/BugfixedHL-Rebased) dll and many of cvars (commands) in my config doesn't exist in standard client.dll.

**HOW TO DOWNLOAD**: just open any of my configs with gitlab web IDE and copy content.
