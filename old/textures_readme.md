# Textures

![](../../img/description_pic.png)

### [Clean Textures](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/textures/textures.zip)
### [Clean Grid](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/textures/clean_grid_textures.zip)

This is textures that I use. It wasn't me who made those textures but I really like the cleanness that they add to the game. I edited some of the textures in half-life.wad file and added .wad files for some of the maps but that's all I have done with it.

Right now I use [clean_grid_textures](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/textures/clean_grid_textures.zip). They are same as my textures but with grid on it.

##### textures installation:
Put all of the files from textures/ to your half-life/valve/(half-life/ag/).
