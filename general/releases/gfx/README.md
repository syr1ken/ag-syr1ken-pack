# Gfx

![](../../img/description_pic.png)

### [Download link](https://gitlab.com/syr1ken/ag-syr1ken-pack/-/blob/master/releases/gfx/gfx.zip)

This is my gfx directory. All I have here is env/ directory with black sky files. If you want the skybox look pitch black in the game you can download them.

##### black sky installation:
Put all of the files from gfx/env to your half-life/valve/gfx/env(half-life/ag/gfx/env).
